import ColumnStart from "../ColumnComponents/ColumnStart";
import ColumnMain from "../ColumnComponents/ColumnMain";
import ColumnEnd from "../ColumnComponents/ColumnEnd";
import { useState , useEffect} from "react";

const Body = (props) => {
    //--------ALEX DAVILA-----------------------
    //This is for sorting purposes, we sort the users array by the numbers of posts posted
    props.users.sort(function(a, b) {
        let keyA = a.nberPosts,
          keyB = b.nberPosts;
       //here we are comparing two values to see who goes first
        if (keyA < keyB) return 1;
        if (keyA > keyB) return -1;
        return 0;
      });

    //The following lines create two arrays that contain: the first one is the array that contains
    //every single topic, and the second one is the array that contains every single post
    //This is used in the table component to generate the stats
    let topArr = [];
    let postArr = [];
    //topic array
    for(let category of props.category){
        for(let topics of category.topicList){
            topArr.push(topics)
        }
      }
      //sorting the topics by number of posts
      topArr.sort(function(a, b) {
        let keyA = a.listPosts.length,
          keyB = b.listPosts.length;
       //here we are comparing two values to see who goes first
        if (keyA < keyB) return 1;
        if (keyA > keyB) return -1;
        return 0;
      });

      //posts array
      for(let category of props.category){
        for(let topics of category.topicList){
            for(let posts of topics.listPosts){
                postArr.push(posts)
            }
        }
      }
      //sorting the posts from most recent to least
      postArr.sort(function(a, b) {
        let keyA = new Date(a.date),
          keyB = new Date(b.date);
        //here we are comparing two values to see who goes first
        if (keyA < keyB) return 1;
        if (keyA > keyB) return -1;
        return 0;
      });

    return ( 
        
        <section id="body">
            <ColumnStart />
            
            <ColumnMain posts={props.posts} setPosts={props.setPosts} 
            post1={props.post1} post2={props.post2}
            all={props.all} setAll={props.setAll}
            handleDelete={props.handleDelete}
            category={props.category} setCat={props.setCat} setTop={props.setTop} cat={props.cat} top={props.top}
            firstTopics={props.firstTopics} topics1={props.topics1} topics2={props.topics2} />

            <ColumnEnd posts={props.posts} setPosts={props.setPosts} 
            post1={props.post1} post2={props.post2} allPosts={postArr}
            all={props.all} setAll={props.setAll}
            handleDelete={props.handleDelete}
            category={props.category} setCat={props.setCat} setTop={props.setTop}
            firstTopics={props.firstTopics} topics1={props.topics1} topics2={props.topics2} allTopics={props.allTopics}
            users={props.users} topics={topArr}/>
        </section>
        
     );
     
}
 
export default Body;
//--------------------ALEX DAVILA-----------------------------