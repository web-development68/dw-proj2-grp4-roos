//-------- Thomas Roos ---------

/**
 * @description - The websites footer containing pertinent information
 */
const Footer = () => {
    return (  
        <footer>
            <h1>Project 2 - Using React</h1>
            <div>Student1: Alex Davila Alonso, 2132425<br/>Student2: Thomas Roos, 2139198<br/>Student3: Charles-Alexandre Bouchard, 2135704</div>
            <div id="copyright">Copyright © 2022</div>
        </footer>
    );
}
 
export default Footer;