//-------- Thomas Roos ---------
import SearchBar from "../SelectionCmpnts/SearchBar";

/**
 * @description - Creates the header of the website
 * @param {*} props - All the inherited properties of Apps.js
 * @returns - A header along with a Search Bar
 */
const Header = (props) => {
    return (  
        <header>
            <h1 id="page_header">Pseudo Forum</h1>
            <SearchBar posts={props.posts}/>
        </header>
    );
}

export default Header;