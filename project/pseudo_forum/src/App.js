import Header from "./AppComponents/Header";
import Body from "./AppComponents/Body";
import Footer from "./AppComponents/Footer";
import { useState,useEffect } from "react";

function App() {

  //creating all state variables used throughout the program
  let [all,setAll] = useState([]);
  let[category,setCategory] = useState([]);
  let[topics1,setTopics1] = useState();
  let[topics2,setTopics2] = useState();
  let [firstTopics,setFirstTopics] = useState([]);
  let[post1,setPost1] = useState([]);
  let[post2,setPost2] = useState([]);
  let[posts,setPosts] = useState([]);
  let [cat, setCat] = useState([]);
  let [top, setTop] = useState([]);
  let [allTopics,setAllTopics] = useState([]);
  let [allPosts,setAllPosts] = useState([]);

  /**
   * This is a custom hook made to fetch the data in the url provided, inside the fecth we set majority of our state variables
   * @param {Object} url to the forum-data.php
   */
  function useFetchdata(url){
      useEffect(()=>{
          fetch(url)
          .then(resp=>{
              if(resp.status === 200){
                  return resp.json();
              }
          })
          .then(result=>{
              //setting the actual arrays of the php file here in the fetch
              setCategory(result.categories)
              //first topic title
              setTopics1(result.categories[0].topicList[0].topic_title)
              //second topic title
              setTopics2(result.categories[0].topicList[1].topic_title)
              //first topic list
              setFirstTopics(result.categories[0].topicList);
              //list of posts of the first topic
              setPost1(result.categories[0].topicList[0].listPosts)
              //list of posts of the second topic
              setPost2(result.categories[0].topicList[1].listPosts)
              //entire object
              setAll(result)
          })
          .catch((error)=>{
              console.log("Error ", error);
          })
      },[]);
      
      
  }

 
 //url for the forum data
  const url = 'https://sonic.dawsoncollege.qc.ca/~nasro/js320/project2/forum-data.php';
  //url for the user data
  const usrUrl = 'https://sonic.dawsoncollege.qc.ca/~nasro/js320/project2/users-data.php';

  
  let [users,setUSers] = useState([]);
  /**
   * This useEffect fetches the user-data.php file to save all the users into an array
   */
  useEffect(()=>{
    fetch(usrUrl)
    .then(resp=>{
        if(resp.status === 200){
            return resp.json();
        }
    })
    .then(result=>{
       setUSers(result.users)
    })
    .catch((error)=>{
        console.log("Error ", error);
    })
},[]);
  useFetchdata(url);

  /**
   * This function handles the delete feature of our program, when the if of the event is clicked
   * we take out that post from our array and take it out of the all the posts at the same time so it stays deleted
   * and actually reflects on the end column tables
   * @param {} evt 
   */
  const handleDelete = (evt) => {
    let filtered = posts.filter((post)=>{
        return post.id != evt.target.id
    });
    let filtered2 =all.categories[cat.id-1].topicList[top.id-1].listPosts.filter((post)=>{
      return post.id != evt.target.id
    });
    all.categories[cat.id-1].topicList[top.id-1].listPosts = filtered2
    console.log(all.categories[cat.id-1].topicList[top.id-1].listPosts)
    setAll({...all})
    posts = filtered

   setPosts(posts)
   if(posts.length < 1){
    setPosts([])
   }
    

}
  return (
    <>
      <div className="App">
        <Header posts={posts} />
        <Body all={all} setAll={setAll} posts={posts} allPosts={allPosts} setPosts={setPosts} post1={post1} post2={post2}
         category={category} cat={cat} setCat={setCat} top={top} setTop={setTop} handleDelete={handleDelete}
         firstTopics={firstTopics} topics1={topics1} topics2={topics2} allTopics={allTopics}
         users={users}/>
        <Footer />
      </div>
    </>
  );
}

export default App;
