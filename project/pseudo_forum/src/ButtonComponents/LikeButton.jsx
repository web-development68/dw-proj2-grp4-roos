//---------------ALEX DAVILA----------------------
import like from '../assets/images/Like.png';
import dislike from '../assets/images/Dislike.png';
import { useState } from "react";

const LikeButton = (props) => {
    let[likes,setLikes] = useState(0);
    const postLikes = () =>{
    if(props.counter === 0){
      console.log("hello")
      props.post1[props.posts.id - 1].like = props.post1[props.posts.id - 1].like + 1;
      setLikes(props.post1[props.posts.id - 1].like)
    }
    else{
      props.posts.like = props.posts.like + 1;
      setLikes(props.posts.like)
    }
    
        
        
    }
    const postDislikes = () =>{
        if(props.likes+likes > 0){
          // props.posts.like = props.posts.like - 1;
          // setLikes(props.posts.like);
          if(props.counter === 0){
            console.log("hello")
            props.post1[props.posts.id - 1].like = props.post1[props.posts.id - 1].like - 1;
            setLikes(props.post1[props.posts.id - 1].like)
          }
          else{
            props.posts.like = props.posts.like - 1;
            setLikes(props.posts.like)
          }
        }
        //console.log(likes);
    }

   // console.log(props.posts.like)
    //console.log(props.post1.like)
    return (
      <>
       {props.counter === 0 ? (
      <section className="likes">
             Likes: {props.post1[props.posts.id - 1].like}
             <img src={like} alt="like" id="like" onClick={postLikes}/>
            <img src={dislike} alt="Dislike" id="unlike" onClick={postDislikes}/>
          </section>
   ) : (
          <section className="likes">
           Likes: {props.posts.like}
          <img src={like} alt="like" id="like" onClick={postLikes}/>
          <img src={dislike} alt="Dislike" id="unlike" onClick={postDislikes}/>
        </section>
   )}
   </>
        // <section className="likes">
        //       Likes: {props.posts.like}
        //       <img src={like} alt="like" id="like" onClick={postLikes}/>
        //       <img src={dislike} alt="Dislike" id="unlike" onClick={postDislikes}/>
        //     </section>
     );
}
 
export default LikeButton;
//-----------------ALEX DAVILA--------------------