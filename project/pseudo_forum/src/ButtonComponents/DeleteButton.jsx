//-------- Thomas Roos ---------
import { useState,useEffect } from "react";

/**
 * @description - The delete button image
 * @param {*} props 
 * @returns 
 */
const DeleteButton = (props) => {
    return ( 
        <img src={props.delete} alt="delete" className="delete_btn" id={props.post.id} onClick={(e) => {props.handleDelete(e);}}/>
     );
}
 
export default DeleteButton;