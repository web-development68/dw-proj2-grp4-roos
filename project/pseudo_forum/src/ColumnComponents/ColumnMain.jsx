//----ALEX DAVILA --------------------
import TopSettings from "../SelectionCmpnts/TopSettings";
import Delete from "../assets/images/Delete.png";
import { useState, useEffect } from "react";
import ListPosts from "../PostComponents/ListPosts";

const ColumnMain = (props) => {
 
//This useEffect is to set the posts at render time, when the page loads the first list of posts that should be
//displayed is the first list of posts from the first topic
  useEffect(() => {
    if (props.posts.length < 1) {
      props.setPosts(props.post1);
    }
  });
  //this counter is what allows me to know if a change in category or topic has been made
  //if no change then the counter is at 0
  let [counter, setCounter] = useState(0);
 
  return (
    <section id="main_column">
      <TopSettings
      counter={counter}
        setCounter={setCounter}
        setPosts={props.setPosts}
        category={props.category}
        post1={props.post1}
        firstTopics={props.firstTopics}
        topics1={props.topics1}
        topics2={props.topics2}
        cat={props.cat}
        top={props.top}
        setCat={props.setCat}
        setTop={props.setTop}
      />
      <ListPosts posts={props.posts} post1={props.post1} setPosts={props.setPosts}
        counter={counter}
        delete={Delete} handleDelete={props.handleDelete}
        all={props.all} setAll={props.setAll}
         />
    </section>
  );
};

export default ColumnMain;