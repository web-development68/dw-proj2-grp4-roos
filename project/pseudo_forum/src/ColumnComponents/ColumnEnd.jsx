import TableComponent from "../TabledComponents/TableComponent";
import TableComponent2 from "../TabledComponents/TableComponent2";
import TableComponent3 from "../TabledComponents/TableComponent3";

/**
 * @description - Generates the statistics panels containing information on the forum
 * @param {*} props - Properties inherited from Body.jsx
 * @returns 
 */
const ColumnEnd = (props) => {
    return (  
        
        // ---------- Thomas Roos / Charles-Alexandre Bouchard / Alex Davila Alonso----------
        <section id="end_column">
            <TableComponent title="Topic Stats" headers={["Topic Title","# Posts", "Status"]}  topics={props.topics}/>
            <TableComponent2 title="Recent Posts" headers={["Author","Date", "Rate"]}  allPosts={props.allPosts}/>
            <TableComponent3 title="User Stats" users={props.users}/>
        </section>
    );
}
 
export default ColumnEnd;