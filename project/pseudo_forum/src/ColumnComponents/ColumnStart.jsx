/**
 * @description - Creates a dummy admin panel with no real purpose
 * @returns - The admin panel section
 */
const ColumnStart = () => {
    return (  
        //---------- Thomas Roos ----------
        <section id="start_column">
            <section id="admin_panel">
                <div id="title">A D M I N</div>
                <div id="buttons">
                    <button>Create Category</button>
                    <button>Create Topic</button>
                    <button>Close Topic</button>
                    <button>Delete Topic</button>
                </div>
            </section>
        </section>
    );
}
 
export default ColumnStart;