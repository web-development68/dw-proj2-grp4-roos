import LikeButton from "../ButtonComponents/LikeButton";
import DeleteButton from "../ButtonComponents/DeleteButton";
import Delete from '../assets/images/Delete.png';

const ListPosts = (props) => {
          
    return (
   
    <section id="post_section">
    {props.posts.map((post) => (
      <section className="post-row" key={post.id} id={post.id}>
        <section className="line" id="top_info">
          <section className="text">{post.text}</section>
          <LikeButton likes={post.like} posts={post} post1={props.post1} allPosts={props.posts} counter={props.counter}/>
        </section>

        <section className="line" id="bottom_info">
          <section className="auth">By: {post.author}</section>
          <section className="title">{post.date}</section>
          <section className="replies">Replies: {post.replies}</section>
          <DeleteButton delete={Delete} handleDelete={props.handleDelete} post={post} posts={props.posts} setPosts={props.setPosts} all={props.all} setAll={props.setAll} />
        </section>
      </section>
    ))}
  </section>
    );
  };

  export default ListPosts;