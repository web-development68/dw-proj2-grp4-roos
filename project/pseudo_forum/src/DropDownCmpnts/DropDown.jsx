// -------------------------------ALEX DAVILA-------------------
import { useState , useEffect} from "react";
const DropDown = (props) => {
  let [topics, setTopics] = useState(props.firstTopics);

/**
 * On a category change which is the first select optiosn a list of variables will be set in order to generate
 * posts and topics from the second select
 * @param {*} e 
 */
  const handleChange = (e) => {
    //if we change categories counter goes up by 1
    props.setCounter((count)=>count+1);
    //we set the topics (options of second select)
    setTopics(props.category[e.target.value - 1].topicList);
    
    //to handle the second category that only has one topic and we cant use an onchange on that topic
    if(props.category[e.target.value - 1].topicList.length === 1){
      //setting the current topic on display
      props.setTop(props.category[e.target.value - 1].topicList[0])
    }
    //setting the current category on display
    props.setCat(props.category[e.target.value -1]);
    //set the current posts to be displayed
    props.setPosts(props.category[e.target.value - 1].topicList[props.top.id - 1].listPosts);
  };

  //if no change has been made to the select the counter stays at zero and the default categories and topics are the first of each
  if(props.counter === 0){
    props.setCat(props.category[0])
    props.setTop(props.firstTopics[0])
  }
  
 
/**
 * On a topic change we change what current posts is displayed to the user
 * @param {*} e 
 */
  const handleTopicChange = (e) =>{
    //when a topic change happend add one to the counter
    props.setCounter((count)=>count+1);
    //this if statement checks if a change in category has been made, hence the topics variable has been initialized
    //if not then set posts to the first topic list
    if(topics.length < 1){
      //setting the current posts
        props.setPosts(props.firstTopics[e.target.value - 1].listPosts);
        //setting the current topic
        props.setTop(props.firstTopics[e.target.value - 1])
    }
    else{
      //setting the current post
      props.setPosts(topics[e.target.value - 1].listPosts)
      //set current topic
      props.setTop(topics[e.target.value - 1])
    }

  }

  return (
    <section id="drop_down">
      <div className="drop_selects">
        <label htmlFor="cat">Choose Category</label>
        <select name="category" id="cat" onChange={(e) => handleChange(e)}>
          {props.category.map((cat) => {
            return (
              <option key={cat.id} value={cat.id}>
                {cat.name}
              </option>
            );
          })}
        </select>
      </div>
      <div className="drop_selects">
        <label htmlFor="tops">Choose topic</label>
        <select name="topics" id="tops" onChange={(e) => handleTopicChange(e)}>
          {" "}
          {topics.length < 1 ? (
            <>
              <option key="1" value="1">
                {props.topics1}
              </option>
              <option key="2" value="2">
                {props.topics2}
              </option>
            </>
          ) : (
            topics.map((topic) => {
              return (
                <option key={topic.id} value={topic.id}>
                  {topic.topic_title}
                </option>
              );
            })
          )}
        </select>
      </div>
    </section>
  );
};

export default DropDown;
//-------------------------------ALEX DAVILA--------------------------------------------
