//---------- Thomas Roos ----------
import React, {useState, useEffect} from 'react'

/**
 * @description - Allows the user to search through the website's posts, filters through authors, dates and titles
 * @param {*} props - Inherits the properties of Header.jsx 
 * @returns - A search bar input
 */
const SearchBar = (props) => {
    const [searchInput, setSearch] = useState('');

    const handleChange = (e) => {
        setSearch(e.target.value.toLowerCase());
    };
    
    useEffect(() => {
        getContentToDisplay();
    },[searchInput]);

    function getContentToDisplay(){
        
        // Searching Matching Visiblity (Title/Author/Date)
        if (searchInput.length > 0) {
            props.posts.forEach((post) => {
                let searchText = post.text.toLowerCase();
                let searchAuthor = post.author.toLowerCase();
                let searchDate = post.date;
                if(!searchText.includes(searchInput) && !searchAuthor.includes(searchInput) && !searchDate.includes(searchInput)) {
                    document.getElementById(post.id).style.visibility = "hidden";
                } else {
                    document.getElementById(post.id).style.visibility = "visible";
                }
            });
        } else {
            props.posts.forEach((post) => {
                document.getElementById(post.id).style.visibility = "visible";
            });
        }

        // Row Positioning
        props.posts.forEach((post) => {
            if(window.getComputedStyle(document.getElementById(post.id)).visibility === "hidden") {
                let post_section = document.getElementById("post_section");
                post_section.appendChild(document.getElementById(post.id));
            }
        });
    }
    
    return (  
        <input id="search_bar" type="search" placeholder="search the entire forum" onChange={handleChange} value={searchInput} posts={props.posts}></input>
    );
}
 
export default SearchBar;