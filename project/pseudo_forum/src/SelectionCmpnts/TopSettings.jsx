import DropDown from "../DropDownCmpnts/DropDown";

const TopSettings = (props) => {
   
    return (  
        <section id="top_settings">
            <DropDown setPosts={props.setPosts} counter={props.counter} setCounter={props.setCounter}
             category={props.category}  setCat={props.setCat} setTop={props.setTop} cat={props.cat} top={props.top}
             firstTopics={props.firstTopics} topics1={props.topics1} topics2={props.topics2}/>
        </section>
    );
}
 
export default TopSettings;