const TableComponent = (props) => {
  //=-----------------ALEX DAVILA--------------------------------
  return (  
    <section id="tableComponent1">
      <h2>{props.title}</h2>
        <table id={props.title}>
          <tbody>
            <tr>
              {props.headers.map((header) => (
                <th>{header}</th>
              ))}
            </tr>
              {
                props.topics.map(topic => {
                 return <tr>
                      <td>{topic.topic_title}</td> <td>{topic.listPosts.length}</td> <td>{topic.status}</td>
                  </tr>
                })
              }
          </tbody>
        </table>
    </section>
  );
}
 
export default TableComponent;
//=-----------------ALEX DAVILA--------------------------------