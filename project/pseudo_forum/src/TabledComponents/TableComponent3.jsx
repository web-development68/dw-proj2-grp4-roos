const TableComponent3 = (props) => {
  //=-----------------ALEX DAVILA--------------------------------
    return (  
      <section id="tableComponent3">
        <h2>{props.title}</h2>
          <table id={props.title}>
            <tbody>
              <tr>
                <th>name</th> <th>nberPost</th>
              </tr>
                {
                  props.users.map(user => {
                   return <tr>
                        <td>{user.user_id}</td> <td>{user.nberPosts}</td>
                    </tr>
                  })
                }
            </tbody>
          </table>
      </section>
    );
  }
   
  export default TableComponent3;
  //=-----------------ALEX DAVILA--------------------------------