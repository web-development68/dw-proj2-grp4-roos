const TableComponent2 = (props) => {
  //=-----------------ALEX DAVILA--------------------------------
    return (  
      <section id="tableComponent2">
        <h2>{props.title}</h2>
          <table id={props.title}>
            <tbody>
              <tr>
                {props.headers.map((header) => (
                  <th>{header}</th>
                ))}
              </tr>
                {
                  props.allPosts.map(post => {
                   return <tr>
                        <td>{post.author}</td> <td>{post.date}</td> <td>{post.rate}</td>
                    </tr>
                  })
                }
            </tbody>
          </table>
      </section>
    );
  }
   
  export default TableComponent2;
  //=-----------------ALEX DAVILA--------------------------------