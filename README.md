# dw-proj2-grp4-ROOS

Pseudo Forum

Description

This web application takes in php files, fetches them and then renders posts and statistic tables, the search bar can be use to look for specific posts by turning them visible or hidden depending on the content of the posts. The posts consist of a title, an author, the date posted, and the number of likes and replies. They can be deleted, liked or disliked which adds or removes from the number of likes.
The tables in the side column consist of the Topic Stats table, which gives information about each topic, the Recent Posts table, which shows the most recent posts and information about them, and finally the Users stats table, which shows the number of posts each user has posted.

This app was developed in React, CSS, and JSX

Installation/Running the Program

Install Node.js, and then open the application itself by opening a terminal in the Path where the package file of the web application is contained, and run the command "npm start". A page will then open and the web application will have started.

Credits/Contributors

Thomas Roos, 2139198
Alex Davila Alonso, 2132425
Charles-Alexandre Bouchard, 2135704

DawsonCollege, CS Dept. 420-320-DW Web Development II Section 1

License

MIT License

Copyright (c) 2022 Thomas Roos, Alex Davila Alonso, Charles-Alexandre Bouchard

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limiation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substandial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.